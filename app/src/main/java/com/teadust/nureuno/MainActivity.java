package com.teadust.nureuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton btnUno = (ImageButton) findViewById(R.id.btn_uno);
        btnUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent threeChoiceIntent = new Intent(MainActivity.this, ThreeChoicesActivity.class);
                startActivity(threeChoiceIntent);
            }
        });
    }
}
