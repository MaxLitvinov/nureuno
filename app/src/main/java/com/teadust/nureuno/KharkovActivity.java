package com.teadust.nureuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class KharkovActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kharkov);

        Button btnSocialDepartment = (Button) findViewById(R.id.btn_social_protection_department);
        btnSocialDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent socialDepartmentIntent = new Intent(KharkovActivity.this, DepartmentOfSocialProtectionActivity.class);
                startActivity(socialDepartmentIntent);
            }
        });
    }
}
