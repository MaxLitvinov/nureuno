package com.teadust.nureuno;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.teadust.nureuno.adapters.SocialProtectionAdapter;

import java.util.ArrayList;
import java.util.List;

public class DepartmentOfSocialProtectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department_of_social_protection);

        List<String> departments = new ArrayList<String>();
        departments.add("1. Алексеевский район");
        departments.add("2. Гончаровка");
        departments.add("3. Дзержинский район");
        departments.add("4. Заиковка");
        departments.add("5. Коминтерноский район");
        departments.add("6. Красный октябрь");
        departments.add("7. Криворожский район");
        departments.add("8. Левада");
        departments.add("9. Ленинский район");
        departments.add("10. Лесопарк");
        departments.add("11. Московский район");
        departments.add("12. Новая Бавария");
        departments.add("13. Новые Дома");
        departments.add("14. Новожаново");
        departments.add("15. Новоселовка");
        departments.add("16. Октябрьский район");
        departments.add("17. Орджоникидзевский район");
        departments.add("18. Павлово Поле");
        departments.add("19. Песочин");
        departments.add("20. Поселок Жуковского");
        departments.add("21. Салтовка");
        departments.add("22. Северная");
        departments.add("23. Сосновая горка");
        departments.add("24. Университетская горка");
        departments.add("25. Фрунзенский район");
        departments.add("26. Холодная Гора");
        departments.add("27. Червонозаводский район");

        SocialProtectionAdapter adapter = new SocialProtectionAdapter(this, departments);

        ListView lvDepartments = (ListView) findViewById(R.id.lv_department);
        lvDepartments.setAdapter(adapter);
        lvDepartments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    Intent departmentInfoIntent = new Intent(DepartmentOfSocialProtectionActivity.this,
                            DepartmentInfoActivity.class);
                    startActivity(departmentInfoIntent);
                }
            }
        });
    }
}
