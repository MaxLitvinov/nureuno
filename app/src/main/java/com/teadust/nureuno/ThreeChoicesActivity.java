package com.teadust.nureuno;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ThreeChoicesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three_choices);

        Button btnTitle = (Button) findViewById(R.id.btn_title);
        btnTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Button btnDictionary = (Button) findViewById(R.id.btn_dictionary);
        btnDictionary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dictionaryIntent = new Intent(ThreeChoicesActivity.this, DictionaryActivity.class);
                startActivity(dictionaryIntent);
            }
        });

        Button btnRegion = (Button) findViewById(R.id.btn_region);
        btnRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regionIntent = new Intent(ThreeChoicesActivity.this, RegionsActivity.class);
                startActivity(regionIntent);
            }
        });
    }
}
