package com.teadust.nureuno;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.teadust.nureuno.adapters.DictionaryAdapter;

import java.util.HashMap;
import java.util.Map;

public class DictionaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("Слово", "Word");
        dictionary.put("Собака", "Dog");
        dictionary.put("Кошка", "Cat");
        dictionary.put("Простой", "Simple");
        dictionary.put("Слушать", "Listen");
        dictionary.put("Метро", "Underground");
        dictionary.put("ООН", "UNO");
        dictionary.put("Любовь", "Love");
        dictionary.put("Хранитель", "Guardian");
        dictionary.put("Щит", "Shield");
        dictionary.put("Критерий", "Criteria");
        dictionary.put("Барбосс", "Barbossa");
        dictionary.put("Костюм", "Suit");
        dictionary.put("Растение", "Plant");
        dictionary.put("Стол", "Table");
        dictionary.put("Неистовство", "Fury");
        dictionary.put("Дуб", "Oak");
        dictionary.put("Зкмля", "Ground");
        dictionary.put("Бесконечный", "Never-end");
        dictionary.put("Песок", "Sand");
        dictionary.put("Бежать", "Run");
        dictionary.put("Спать", "Sleep");
        dictionary.put("Идти", "Go");
        dictionary.put("Окно", "Window");
        dictionary.put("Гитара", "Guitar");
        dictionary.put("Заметка", "Note");

        DictionaryAdapter adapter = new DictionaryAdapter(dictionary);

        ListView lvDictionary = (ListView) findViewById(R.id.lv_dictionary);
        lvDictionary.setAdapter(adapter);
    }
}
