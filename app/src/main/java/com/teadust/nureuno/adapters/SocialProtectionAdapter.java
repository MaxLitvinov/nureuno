package com.teadust.nureuno.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teadust.nureuno.R;

import java.util.List;

public class SocialProtectionAdapter extends ArrayAdapter<String> {

    public SocialProtectionAdapter(Context context, List<String> departments) {
        super(context, R.layout.social_protection_short_view, departments);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String department = getItem(position);
        DepartmentHolder departmentHolder;
        if (convertView == null) {
            departmentHolder = new DepartmentHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.social_protection_short_view, parent, false);
            departmentHolder.tvDepartment = (TextView) convertView.findViewById(R.id.tv_department);
            departmentHolder.dividerLine = convertView.findViewById(R.id.divider_line);
            convertView.setTag(departmentHolder);
        } else {
            departmentHolder = (DepartmentHolder) convertView.getTag();
        }
        departmentHolder.tvDepartment.setText(department);
        return convertView;
    }

    private class DepartmentHolder {

        public TextView tvDepartment;
        public View dividerLine;
    }
}
