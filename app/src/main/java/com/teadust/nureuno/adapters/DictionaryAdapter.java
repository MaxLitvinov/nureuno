package com.teadust.nureuno.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teadust.nureuno.R;

import java.util.ArrayList;
import java.util.Map;

public class DictionaryAdapter extends BaseAdapter {

    private final ArrayList mData;

    public DictionaryAdapter(Map<String, String> map) {
        this.mData = new ArrayList();
        this.mData.addAll(map.entrySet());
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, String> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DictionaryHolder dictionaryHolder;

        if (convertView == null) {
            dictionaryHolder = new DictionaryHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.dictionary_short_view, parent, false);
            dictionaryHolder.word = (TextView) convertView.findViewById(R.id.tv_word);
            dictionaryHolder.translation = (TextView) convertView.findViewById(R.id.tv_translation);
            convertView.setTag(dictionaryHolder);
        } else {
            dictionaryHolder = (DictionaryHolder) convertView.getTag();
        }

        Map.Entry<String, String> item = getItem(position);

        dictionaryHolder.word.setText(item.getKey());
        dictionaryHolder.translation.setText(item.getValue());

        return convertView;
    }

    private class DictionaryHolder {

        public TextView word;
        public TextView translation;
    }
}
